<?
namespace Bitrix;

use Bitrix\Im\Call\Auth;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Web;
use Bitrix\Main\CUser;
use DateInterval;
use DateTime;
use Bitrix\Main\Session\Session;

class Loyalty{

    private static \Bitrix\Main\Web\HttpClient $httpClient;
	private static $loyaltyHost = "https://loyalfans.rubin-kazan.ru";

    public function __construct()
	{
	}

	public function getCurrentUser($bearer)
	{
		$httpClient = self::getHttpClient($bearer);
		
		$jsonResponse = $httpClient->get(self::$loyaltyHost . "/api/v1/users/current");
		$status = $httpClient->getStatus();
	
		if($status != 200) return null;
	
		try{
			$arResult = \Bitrix\Main\Web\Json::decode($jsonResponse);
		}
		catch(ArgumentException $e){
			return null;
		}
		if(is_array($arResult)){
			if(array_key_exists('error', $arResult)) {
				$result = $arResult['error'];
				return array('status' => $status, 'message' => $result);
			}
			if(array_key_exists('message', $arResult)){
				$result = $arResult['message'];
				return array('status' => $status, 'message' => $result);
			}
		}
		return $arResult;
	}
	
	public function retryAuth($refresh){	
		$httpClient = self::getHttpClient();
		$jsonResponse = $httpClient->post(self::$loyaltyHost . "/api/v1/Authorization", json_encode([
			"refreshToken"=>$refresh,
		]));
	
		try{
			$arResult = \Bitrix\Main\Web\Json::decode($jsonResponse);
			return [
				"accessToken" => $arResult['accessToken'],
				"refreshToken" => $arResult['refreshToken']
			];
		}
		catch(ArgumentException $e){
			return null;
		}
		return null;
	}

	public function isAuthorized($bearer){
		$httpClient = self::getHttpClient($bearer);
        $jsonResponse = $httpClient->get(self::$loyaltyHost . "/api/v1/Authorization/isAuthorized");
		$status = $httpClient->getStatus();
		$arResult = \Bitrix\Main\Web\Json::decode($jsonResponse);
        return $status == 200 && $arResult == "true";
	}

	private static function getHttpClient($authHeader = null)
    {
        if (!isset(self::$httpClient)) {
            self::$httpClient = new \Bitrix\Main\Web\HttpClient();
			self::$httpClient->setHeader('Content-type', 'application/json');
        }
		if($authHeader != null)
			self::$httpClient->setHeader('Authorization', "Bearer " . $authHeader);

		self::$httpClient->setRedirect(false);
        return self::$httpClient;
    }
}

