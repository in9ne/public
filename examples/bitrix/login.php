<?php 
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/api/loyalty/loyalty.php");

global $APPLICATION;
global $USER;


$currentDomain = (CMain::IsHTTPS() ? "https://" : "http://") . $_SERVER['HTTP_HOST'];
$loyaltyHost = "https://loyalfans.rubin-kazan.ru";
$loyaltyClient = new \Bitrix\Loyalty();

if($_SERVER['REQUEST_METHOD'] === 'GET') {
    if(isset($_GET['token']) && !empty($_GET['token'])){
        $tokenResponce = $loyaltyClient->retryAuth(base64_decode($_GET['token']));
        $bearer = $tokenResponce["accessToken"];
        $APPLICATION->set_cookie('LF-token', $bearer, time()+60*60*8);
        auth($bearer, $loyaltyClient);
    }
    else{
        $bearer = $APPLICATION->get_cookie("LF-token");
        $refresh = base64_decode($_COOKIE['token']);

        if(empty($bearer) && !empty($refresh)){
            $tokenResponce = $loyaltyClient->retryAuth($refresh);
            $bearer = $tokenResponce["accessToken"];
            $APPLICATION->set_cookie('LF-token', $bearer, time()+60*60*8);
            auth($bearer, $loyaltyClient);
        }
        else if(!empty($bearer) && empty($refresh)){
            $res = auth($bearer, $loyaltyClient);
            if($res == null){
                ResetCookies();
                LocalRedirect($loyaltyHost . "?callbackUrl=". $currentDomain . "/api/login.php", true);
            }
        }
        else{
            LocalRedirect($loyaltyHost . "?callbackUrl=". $currentDomain . "/api/login.php", true);
        }
    }
    LocalRedirect("/", true);
}

function resetCookies(){
    global $APPLICATION;
    $APPLICATION->set_cookie('LF-token', "", 0);
}

function auth($bearer, $loyaltyClient){
    $arResult = $loyaltyClient->getCurrentUser($bearer);
    if(!empty($arResult)){
        $login = $arResult["login"];
        $arFields = Array(
            "LOGIN" => $login,
            "NAME" => $arResult["firstName"],
            "LAST_NAME" => $arResult["lastName"],
            "PASSWORD" => rand(10000000,99999999),
            "EMAIL" => !empty($arResult["email"]) ? $arResult["email"] : $login.$_SERVER['HTTP_HOST'],
            "ACTIVE" => "Y",
            "XML_ID"=>$arResult["id"],
            "LID" => SITE_ID,
            "PERSONAL_BIRTHDAY" =>!empty($arResult["birthDate"]) ? date('d.m.Y', strtotime($arResult["birthDate"])) : null,
            "PERSONAL_CITY" => !empty($arResult["city"]) ? $arResult["city"] : null,
            "PERSONAL_AREA" => !empty($arResult["area"]) ? $arResult["area"] : null
            );
        $oUser = new CUser;
    
        $res = CUser::GetByLogin($login);
        if($arUser = $res->Fetch()){
            $ID = $arUser["ID"];
            $oUser->Update($ID, $arFields);
        }
        else
        {
            $ID = $oUser->Add($arFields);
        }

        if($ID > 0)
        {
            global $USER;
            $USER->Authorize($ID);
        }
    }
    else{
        global $APPLICATION;
        $refresh = $APPLICATION->get_cookie('token');

        if($refresh != ""){
            $res = $loyaltyClient->reTryAuth($refresh);
            if($res == null) return null;
            else auth($res["accessToken"], $loyaltyClient);
        }
    }
}
