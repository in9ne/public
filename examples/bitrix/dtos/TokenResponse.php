<?
namespace Bitrix\Loyalty;

class TokenResponse
{
	public string $accessToken;
	public string $refreshToken;
    public string $expairedAt;

    public function __construct(
		string $accessToken,
		string $refreshToken
	) {
		$this->accessToken = $accessToken;
		$this->refreshToken = $refreshToken;
	}

}