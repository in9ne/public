<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/api/loyalty/loyalty.php");

$currentDomain = (CMain::IsHTTPS() ? "https://" : "http://") . $_SERVER['SERVER_NAME'];

global $USER;
global $APPLICATION;
$loyaltyClient = new \Bitrix\Loyalty();
$loyaltyHost = "https://loyalfans.rubin-kazan.ru";

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET["p"]) &&  $_GET["p"] === "logout") {
    $USER->Logout();
    //TODO: api logout
    LocalRedirect('/');
}
if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET["p"]) &&  $_GET["p"] === "current") {

    $token = $APPLICATION->get_cookie("LF-token");
    $refresh = base64_decode($_COOKIE["token"]);

    if(empty($token) || empty($refresh)){
        $USER->Logout();
        LocalRedirect($loyaltyHost . '?callbackUrl='. $currentDomain . '/api/login.php');
    }
    else{
        if($loyaltyClient->isAuthorized($token)){
            LocalRedirect($loyaltyHost . '/Participant/Profile');
        }
        else{
            $bearer = $loyaltyClient->reTryAuth($refresh);
            //TODO send refresh front 
        }
    }
}
